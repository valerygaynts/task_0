//
// Created by valery on 25.02.2020.
//

#ifndef TASK_BOX_H
#define TASK_BOX_H

#include <ostream>

#
struct Box {
private:
  int lenght, widht, height, value;
  double weight;

public:
  Box(int lenght, int widht, int height, int value, float weight);
  Box();
  bool operator<=(const Box &rhs) const { return !(rhs < *this); }

  bool operator>=(const Box &rhs) const { return !(*this < rhs); }
  int getLenght() const;

  void setLenght(int lenght);

  int getWidht() const;

  void setWidht(int widht);

  int getHeight() const;

  void setHeight(int height);

  int getValue() const;

  void setValue(int value);

  float getWeight() const;

  void setWeight(float weight);

  friend std::ostream &operator<<(std::ostream &os, const Box &box);

  friend std::istream &operator>>(std::istream &in, Box &box);

  bool operator<(const Box &rhs) const;

  bool operator>(const Box &rhs) const;

  bool operator==(const Box &rhs) const;
};

#endif // TASK_BOX_H
