//
// Created by valer on 13.04.2020.
//

#ifndef TASK_0_CONTAINER_H
#define TASK_0_CONTAINER_H

#include <vector>
#include "Box.h"
class Container {
private:
  std::vector<Box> boxes;
  int length;
  int width;
  int height;
  double maxW;

public:
  Container(int length, int width, int height, double maxW);
  void add(Box box, int index);
  void add(Box box);
  void deleted(int index);
  void deleted();
  int quantity();
  double totalWeight();
  int totalCost();
  double getWeight();

  friend std::ostream& operator<<(std::ostream &os, const Container &container);

  friend std::istream& operator>>(std::istream &in, Container &container);
  Container(Box box, Box box1, Box box2);
};

#endif // TASK_0_CONTAINER_H
