//
// Created by valery on 25.02.2020.
//

#include "../headers/Box.h"
#include <iostream>

Box::Box(int lenght, int widht, int height, int value, float weight) {
  this->lenght = lenght;
  this->widht = widht;
  this->height = height;
  this->value = value;
  this->weight = weight;
}

Box::Box() {
  this->lenght = 0;
  this->widht = 0;
  this->height = 0;
  this->value = 0;
  this->weight = 0;
}

std::istream& operator>> (std::istream &in, Box &box) {
  in >> box.lenght;
  in >> box.widht;
  in >> box.height;
  in >> box.value;
  in >> box.weight;
  return in;
}


std::ostream& operator<<(std::ostream &os, const Box &box) {
  os << "lenght: " << box.lenght << " widht: " << box.widht << " height: " << box.height << " value: " << box.value
     << " weight: " << box.weight;
  return os;
}

int Box::getLenght() const {
  return lenght;
}

void Box::setLenght(int lenght) {
  Box::lenght = lenght;
}

int Box::getWidht() const {
  return widht;
}

void Box::setWidht(int widht) {
  Box::widht = widht;
}

int Box::getHeight() const {
  return height;
}

void Box::setHeight(int height) {
  Box::height = height;
}

int Box::getValue() const {
  return value;
}

void Box::setValue(int value) {
  Box::value = value;
}

float Box::getWeight() const {
  return weight;
}

void Box::setWeight(float weight) {
  Box::weight = weight;
}

bool Box::operator<(const Box &rhs) const {
  return lenght < rhs.lenght && widht < rhs.widht && height < rhs.height;
}

bool Box::operator>(const Box &rhs) const {
  return lenght > rhs.lenght && widht > rhs.widht && height > rhs.height;
}

bool Box::operator==(const Box &rhs) const {
  return lenght == rhs.lenght && widht == rhs.widht && height == rhs.height &&
         value == rhs.value && weight == rhs.weight;
}





