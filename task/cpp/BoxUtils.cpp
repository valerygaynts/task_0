#include "../headers/Box.h"

class BoxUtils {
  /** #2
   *  для массива коробок вычисляет суммарную стоимость
   *  содержимого всех коробок
   * @param array
   * @param size
   * @return
   */
public:
  long calculateTotalCost(Box *array, unsigned size) {
    long sum = 0;
    for (unsigned i = 0; i < size; i++) {
      sum += array[i].getValue();
    }
    return sum;
  }
  /**
   * #3
   *  проверяет, что сумма длины, ширины и высоты всех коробок
   *  не превосходит заданного значения.
   * @param maxValue
   * @param array
   * @param size
   * @return
   */
  bool maxValue(Box *array, unsigned size, unsigned maxValue){
    float sum = 0 ;
    for (int i =0; i < size; i++){
      sum += array[i].getLenght();
      sum += array[i].getWidht();
      sum += array[i].getHeight();
    }
    return sum <= maxValue;
  }
  /** #4
   *  функцию, которая определяет максимальный вес коробок, объем которых не
   *  больше параметра maxV
   * @param array
   * @param maxV
   * @param size
   * @return
   */
  float maxWeight(Box *array, unsigned size, unsigned maxV) {
    float maxWeight = 0;
    for(unsigned i = 0; i < size; i++){
      if((array[i].getLenght() * array[i].getWidht() * array[i].getHeight()) <= maxV){
        maxWeight += array[i].getWeight();
      }
    }
    return maxWeight;
  }
/** #5
 *  проверяет, что все
 *  коробки массива можно вложить друг в друга по одной штуке
 * @param array
 * @param size
 * @return
 */
  bool boxInBox(Box *array, unsigned size) {
    Box temp;
    int count = 0;
    for (int i = 0; i < size - 1; i++) {
      for (int j = 0; j < size - i - 1; j++) {
        if ((array[j].getLenght() * array[j].getWidht() * array[j].getHeight()) > (array[j+1].getLenght() * array[j+1].getWidht() * array[j+1].getHeight())) {
          temp = array[j];
          array[j] = array[j + 1];
          array[j + 1] = temp;
        }
      }
    }
    for (int i = 0; i < size-1; i++ ){
      if(array[i] < array[i+1]){
        count++;
      }
    }
    return count == size - 1;
  }

  bool equals(Box box1, Box box2) {
    if ((box1.getLenght() == box2.getLenght()) && (box1.getWidht() == box2.getWidht()) &&
        (box1.getHeight() == box2.getHeight())) {
      return true;
    }
  }

};
