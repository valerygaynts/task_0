//
// Created by valer on 13.04.2020.
//

#include "../headers/Container.h"
Container::Container(int length, int width, int height, double maxW) {
  this->length = length;
  this->width = width;
  this->height = height;
  this->maxW = maxW;
}

void Container::add(Box box, int index) {
  if ((box.getWeight() + totalWeight() - boxes[index].getWeight()) <= maxW) {
    boxes[index] = box;
  }
}

void Container::add(Box box) {
  if ((box.getWeight() + totalWeight()) <= maxW) {
    boxes.push_back(box);
  }
}
void Container::deleted(int index) {
  if (boxes.size() >= 1) {
    boxes.erase(boxes.begin() + index);
  }
}
void Container::deleted() {
  if (boxes.size() >= 1) {
    boxes.pop_back();
  }
}
int Container::quantity() { return boxes.size(); }

double Container::totalWeight() {
  double totalWeight = 0;
  for (int i = 0; i < boxes.size() - 1; i++) {
    totalWeight += boxes[i].getWeight();
  }
  return totalWeight;
}

int Container::totalCost() {
  int totalCost = 0;
  for (int i = 0; i < boxes.size() - 1; i++) {
    totalCost += boxes[i].getValue();
  }
  return totalCost;
}

/*std::istream& operator>> (std::istream &in, Container &container) {
  in >> box.lenght;
  in >> box.widht;
  in >> box.height;
  in >> box.value;
  in >> box.weight;
  return in;
}*/


std::ostream& operator<<(std::ostream &os, const Container &container) {
  for (int i=0; i < container.boxes.size()-1; i++){
    Box box = container.boxes[i];
    os << box;
  }
  return os;
}