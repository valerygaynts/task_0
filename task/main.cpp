#include <iostream>
#include "headers/Box.h"
#include "headers/Container.h"
#include "cpp/BoxUtils.cpp"
using namespace std;
int main() {
  BoxUtils bu;
  Box box1;
  Box box2(1, 2, 3, 100, 5.5);
  Box box3(1, 2, 3, 100, 5.5);
  cout << (box2 == box3) << endl;
  cout << (box1 == box2) << endl;
  cout << (box1 < box2) << endl;
  cout << (box3 > box1) << endl;

  int size = 6;
  Box array[] = {Box(1, 2, 3, 500, 8.8), Box(2, 3, 6, 88, 9.9),
                 Box(3, 6, 8, 96, 7.6),  Box(4, 7, 9, 96, 7.6),
                 Box(5, 8, 10, 96, 7.6), Box(6, 9, 11, 96, 7.6)};

  cout << bu.calculateTotalCost(array, size) << endl;
  cout << bu.maxValue(array, size, 10) << endl;
  cout << bu.maxValue(array, size, 100) << endl;
  cout << bu.maxWeight(array, size, 70) << endl;
  cout << bu.boxInBox(array, size) << endl;
 // Container container = {box1, box2, box3};
}